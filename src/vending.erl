-module(vending).
-behaviour(gen_statem).

-export([init/1, callback_mode/0, start_link/0, start_link/1, terminate/3, code_change/4]).
-export([take_coins/0, take_products/0, current_balance/0, insert_coin/1, checkout/1, cancel/0]).
-export([handle_event/4]).

-define(ACCEPTABLE_COINS, [200, 100, 50, 20, 10, 5, 2, 1]).
-define(AVAILABLE_PRODUCTS,
  #{
    lemonade => 249,
    chocolate => 199,
    bubblegum => 79,
    smoothie => 273
  }).
-define(MAX_BALANCE, 5000).
-define(DEFAULT_TIMEOUT, 30000).

%%%===================================================================
%%% Behaviour API
%%%===================================================================
-spec start_link() -> gen_statem:init_result([]).
start_link() ->
  gen_statem:start_link({local, ?MODULE}, ?MODULE, {idle, ?DEFAULT_TIMEOUT}, []).

-spec start_link(pos_integer()) -> gen_statem:init_result([]).
start_link(Timeout) ->
  gen_statem:start_link({local, ?MODULE}, ?MODULE, {idle, Timeout}, []).

-spec callback_mode() -> gen_statem:callback_mode_result().
callback_mode() -> handle_event_function.

% default state = idle | waiting_for_nex_action
% default data = 0 balance, empty products and coins output slots
-spec init({_, _}) -> {ok, _, #{balance := 0, coins_output := [], products_output := [], timeout := _}}.
init({State, Timeout}) -> {ok, State, #{balance => 0, products_output => [], coins_output => [], timeout => Timeout}}.

-spec terminate(normal | shutdown | {shutdown, term()} | term(), term(), term()) -> ok.
terminate(_Reason, _State, _Data) ->
  ok.

-spec code_change(term(), term(), term(), term()) -> {ok, term(), term()}.
code_change(_OldVsn, State, Data, _Extra) ->
  {ok, State, Data}.

%%%===================================================================
%%% Public API
%%%===================================================================
% Returns the list of coins stored in the coin output slot and
% clears it
-spec take_coins() -> [integer()].
take_coins() -> gen_statem:call(?MODULE, take_coins).

% Returns the list of products stored in the product output slot and
% clears it
-spec take_products() -> [atom()].
take_products() -> gen_statem:call(?MODULE, take_products).

% Returns the total amount of money that was accepted by the
% machine so far.
-spec current_balance() -> integer().
current_balance() -> gen_statem:call(?MODULE, current_balance).

% Inserts a coin with the value Value into the input slot
-spec insert_coin(integer()) -> accepted | rejected.
insert_coin(Value) -> gen_statem:call(?MODULE, {insert_coin, Value}).

% Checks out the given product from the vending machine.
-spec checkout(atom()) -> accepted | rejected.
checkout(Product) -> gen_statem:call(?MODULE, {checkout, Product}).

% Cancels the current transaction. Clears the balance, returns coins.
-spec cancel() -> ok.
cancel() -> gen_statem:call(?MODULE, cancel).

%%%===================================================================
%%% Callback API
%%%===================================================================
-spec handle_event(gen_statem:event_type(), atom(), [], map()) -> gen_statem:event_handler_result([]).
% returns all coins in a slot, clears slot
handle_event({call, From}, take_coins, _State, #{coins_output := COutput} = Data) ->
  {keep_state, Data#{coins_output => []}, reply_with(From, COutput)};
% returns all products in a slot, clears slot
handle_event({call, From}, take_products, _State, #{products_output := POutput} = Data) ->
  {keep_state, Data#{products_output => []}, reply_with(From, POutput)};
% returns current balance
handle_event({call, From}, current_balance, _State, #{balance := Balance}) ->
  {keep_state_and_data, reply_with(From, Balance)};
% If the coin is accepted, the function will return accepted
% and increase the balance accordingly.
% If the coin is not accepted, the function will return rejected
% and put the inserted coin into the coin output slot.
handle_event({call, From}, {insert_coin, Value}, _State, _Data) when Value < 1 ->
  {keep_state_and_data, reply_rejected(From)};
handle_event({call, From}, {insert_coin, Value}, _State, #{balance := Balance}) when Balance + Value > ?MAX_BALANCE ->
  {keep_state_and_data, reply_rejected(From)};
handle_event({call, From}, {insert_coin, Value}, _State, #{balance := Balance, coins_output := COutput, timeout := Timeout} = Data) ->
  case lists:member(Value, ?ACCEPTABLE_COINS) of
    true ->
      {next_state, waiting_for_next_action, Data#{balance => Value + Balance},
          reply_accepted(From) ++ [{state_timeout, Timeout, cancel}]};
    _ -> {keep_state, Data#{coins_output => [Value | COutput]}, reply_rejected(From)}
  end;
% If the current balance is sufficient for the selected product:
%  - The product will be added to the product output slot.
% If the user overpaid, the difference will be added to the coin
% output slot.
%  - The function returns accepted.
% If the balance is insufficient or the product does not exist:
%  - The balance will stay be unaffected.
%  - The function returns rejected.
handle_event({call, From}, {checkout, Product}, _State, #{balance := Balance} = Data) ->
  case maps:get(Product, ?AVAILABLE_PRODUCTS, not_found) of
    not_found -> {keep_state_and_data, reply_rejected(From)};
    Price -> case Price =< Balance of
               true -> {next_state, idle, checkout_data(Product, Price, Data), reply_accepted(From)};
               _ -> {keep_state_and_data, reply_rejected(From)}
             end
  end;
% cancel, returns current balance into coins slot
handle_event({call, From}, cancel, _State, Data) ->
  {next_state, idle, cancel_data(Data), [{reply, From, ok}]};
% timeout on waiting for next action, performs canceling and returns machine to idle state
handle_event(state_timeout, cancel, waiting_for_next_action, Data) ->
  {next_state, idle, cancel_data(Data)};

% ignore unexpected events
handle_event({call, _From}, Content, State, Data) ->
  io:format("Unexpected content: ~p, state: ~p, data: ~p~n", [Content, State, Data]),
  {next_state, State, Data};
handle_event(Event, Content, State, Data) ->
  io:format("Unexpected event: ~p, content: ~p, state: ~p, data: ~p~n", [Event, Content, State, Data]),
  {next_state, State, Data}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
-spec reply_accepted(pid()) -> [{reply, pid(), accepted}].
reply_accepted(From) ->
  [{reply, From, accepted}].

-spec reply_rejected(pid()) -> [{reply, pid(), rejected}].
reply_rejected(From) ->
  [{reply, From, rejected}].

-spec reply_with(pid(), term()) -> [{reply, pid(), term()}].
reply_with(From, Value) ->
  [{reply, From, Value}].

-spec cancel_data(#{balance := non_neg_integer(), coins_output := _, _ => _}) -> #{balance := 0, coins_output := _, _ => _}.
cancel_data(#{balance := Balance, coins_output := COutput} = Data) ->
  Data#{balance => 0, coins_output => calculate_coins_output(Balance) ++ COutput}.

% sets balance to 0, puts least amount of coins into coins slot to cover the difference, puts product into its slot
-spec checkout_data(atom(), pos_integer(), map()) -> map().
checkout_data(Product, Price, #{balance := Balance, coins_output := COutput, products_output := POutput} = Data) ->
  Diff = Balance - Price,
  Data#{balance => 0, coins_output => calculate_coins_output(Diff) ++ COutput, products_output => [Product | POutput]}.

-spec calculate_coins_output(integer()) -> list(pos_integer()).
calculate_coins_output(N) when N =< 0 ->
  [];
calculate_coins_output(N) when N > ?MAX_BALANCE ->
  [];
calculate_coins_output(N) ->
  calculate_coins_output(N, [], ?ACCEPTABLE_COINS).

% returns smallest possible list of coins for a given amount of money (in reverse order)
% example: for 599 should return [2,2,5,20,20,50,100,200,200]
-spec calculate_coins_output(integer(), list(), list(pos_integer())) -> list(pos_integer()).
calculate_coins_output(_, Acc, []) ->
  Acc;
calculate_coins_output(Value, Acc, [H | Rest] = CoinsList) ->
  case Value - H >= 0 of
    true -> calculate_coins_output(Value - H, [H | Acc], CoinsList);
    _ -> calculate_coins_output(Value, Acc, Rest)
  end.