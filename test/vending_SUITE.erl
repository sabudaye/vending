-module(vending_SUITE).

-export([suite/0, init_per_suite/1, end_per_suite/1, groups/0, all/0]).
-export([insert_coins/1, purchase/1, cancel_on_timeout/1, cancel_timeout_on_event/1, example_suite/1]).

-include_lib("common_test/include/ct.hrl").

% Common Test API
-spec suite() -> term().
suite() ->
  [{timetrap, {seconds, 60}}].

-spec init_per_suite(term()) -> term().
init_per_suite(Config) ->
  Config.

-spec end_per_suite(term()) -> term().
end_per_suite(_Config) ->
  ok.

-spec groups() -> term().
groups() ->
  [].

-spec all() -> term().
all() ->
  [insert_coins, purchase, cancel_on_timeout, cancel_timeout_on_event, example_suite].

% Test suites
-spec insert_coins(term()) -> ok.
insert_coins(_Config) ->
  {ok, _Pid} = vending:start_link(),
  rejected = vending:insert_coin(0),
  rejected = vending:insert_coin(-1),
  accepted = vending:insert_coin(1),
  accepted = vending:insert_coin(10),
  accepted = vending:insert_coin(100),
  accepted = vending:insert_coin(200),
  rejected = vending:insert_coin(500),
  ok = vending:cancel(),
  [1, 10, 100, 200, 500] = vending:take_coins(),
  [] = vending:take_coins(),
  ok.

-spec purchase(term()) -> ok.
purchase(_Config) ->
  {ok, _Pid} = vending:start_link(),
  accepted = vending:insert_coin(100),
  accepted = vending:insert_coin(200),
  accepted = vending:insert_coin(200),
  accepted = vending:checkout(lemonade),
  [lemonade] = vending:take_products(),
  [1, 50, 200] = vending:take_coins(),
  rejected = vending:checkout(bubblegum),
  [] = vending:take_products(),
  ok.

-spec cancel_on_timeout(term()) -> ok.
cancel_on_timeout(_Config) ->
  % start vending machine with 100 ms max event waiting time
  {ok, _Pid} = vending:start_link(100),
  accepted = vending:insert_coin(100),
  accepted = vending:insert_coin(100),
  accepted = vending:insert_coin(100),
  accepted = vending:insert_coin(200),
  % sleep more than max waiting time
  timer:sleep(110),
  % expect machine to cancel the purchase and return balance
  rejected = vending:checkout(lemonade),
  [] = vending:take_products(),
  % returns least possible amount of coins for given balance
  [100, 200, 200] = vending:take_coins(),
  ok.

-spec cancel_timeout_on_event(term()) -> ok.
cancel_timeout_on_event(_Config) ->
  % start vending machine with 100 ms max event waiting time
  {ok, _Pid} = vending:start_link(100),
  accepted = vending:insert_coin(100),
  accepted = vending:insert_coin(100),
  accepted = vending:insert_coin(200),
  % wait almost max waiting time
  timer:sleep(90),
  % make an action
  accepted = vending:insert_coin(100),
  % wait more to make sure that wait time was more than 100
  timer:sleep(20),
  % purchase still should be possible
  accepted = vending:checkout(bubblegum),
  [bubblegum] = vending:take_products(),
  [1, 20, 200, 200] = vending:take_coins(),
  ok.

-spec example_suite(term()) -> ok.
example_suite(_Config) ->
  {ok, _} = vending:start_link(100),
  rejected = vending:insert_coin(15),
  [15] = vending:take_coins(),
  accepted = vending:insert_coin(1),
  accepted = vending:insert_coin(1),
  accepted = vending:insert_coin(50),
  52 = vending:current_balance(),
  ok = vending:cancel(),
  [2, 50] = vending:take_coins(),
  accepted = vending:insert_coin(2),
  accepted = vending:insert_coin(200),
  rejected = vending:checkout(smoothie),
  202 = vending:current_balance(),
  accepted = vending:insert_coin(100),
  accepted = vending:checkout(smoothie),
  [2, 2, 5, 20] = vending:take_coins(),
  [smoothie] = vending:take_products(),
  accepted = vending:insert_coin(200),
  accepted = vending:checkout(bubblegum),
  accepted = vending:insert_coin(200),
  accepted = vending:checkout(chocolate),
  [1, 1, 20, 100] = vending:take_coins(),
  [chocolate, bubblegum] = vending:take_products(),
  accepted = vending:insert_coin(2),
  timer:sleep(110),
  0 = vending:current_balance(),
  [2] = vending:take_coins(),
  ok.