### Requirements
- Erlang/OTP 22 or higher
- `rebar3` available as executable

### Running tests
```rebar3 ct```

Expected output: ```All 4 tests passed.```

### Usage

1. `cd src`
2. `erl`
3. `c(vending).`
4. `vending:start_link().`
5. `vending:insert_coins(200).`
6. `vending:insert_coins(200).`
7. `vending:current_balance().`
8. `vending:checkout(lemonade).`
9. `vending:take_coins().`
10. `vending:take_products().`

For more options see `test/vending_SUITE.erl`

